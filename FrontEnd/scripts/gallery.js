var gallery = angular.module("main");
gallery.controller('galleryController', function ($scope) {
    $scope.isFolder = function(item) {
        return (item.children != null);
    };

    $scope.getItemClass = function(item) {
        if ($scope.isFolder(item)) return 'folder';
        return 'image';
    };

    $scope.images = {
       children: [
           { name: 'Геленджик зимние каникулы 2015.',
             thumb: '/images/gallery/folder2.png',
             children: [
                 { path: '/images/gallery/gelendgik/1.jpg',
                   thumb: '/images/gallery/gelendgik/thumb/1.jpg'
                 },
                 { path: '/images/gallery/gelendgik/2.jpg',
                   thumb: '/images/gallery/gelendgik/thumb/2.jpg'
                 },
                 { path: '/images/gallery/gelendgik/3.jpg',
                   thumb: '/images/gallery/gelendgik/thumb/3.jpg'
                 },
                 { path: '/images/gallery/gelendgik/4.jpg',
                   thumb: '/images/gallery/gelendgik/thumb/4.jpg'
                 },
                 { path: '/images/gallery/gelendgik/5.jpg',
                   thumb: '/images/gallery/gelendgik/thumb/5.jpg'
                 },
                 { path: '/images/gallery/gelendgik/6.jpg',
                   thumb: '/images/gallery/gelendgik/thumb/6.jpg'
                 },
                 { path: '/images/gallery/gelendgik/7.jpg',
                   thumb: '/images/gallery/gelendgik/thumb/7.jpg'
                 },
                 { path: '/images/gallery/gelendgik/8.jpg',
                   thumb: '/images/gallery/gelendgik/thumb/8.jpg'
                 }
             ]
           },
           {
               name: 'Турция. Ликийская тропа. Октябрь 2014.',
               thumb: '/images/gallery/folder2.png',
               children: [
                   {
                       path: '/images/gallery/turkey/1.jpg',
                       thumb: '/images/gallery/turkey/thumb/1.jpg'
                   },
                   {
                       path: '/images/gallery/turkey/2.jpg',
                       thumb: '/images/gallery/turkey/thumb/2.jpg'
                   },
                   {
                       path: '/images/gallery/turkey/3.jpg',
                       thumb: '/images/gallery/turkey/thumb/3.jpg'
                   },
                   {
                       path: '/images/gallery/turkey/4.jpg',
                       thumb: '/images/gallery/turkey/thumb/4.jpg'
                   },
                   {
                       path: '/images/gallery/turkey/5.jpg',
                       thumb: '/images/gallery/turkey/thumb/5.jpg'
                   },
                   {
                       path: '/images/gallery/turkey/6.jpg',
                       thumb: '/images/gallery/turkey/thumb/6.jpg'
                   },
                   {
                       path: '/images/gallery/turkey/7.jpg',
                       thumb: '/images/gallery/turkey/thumb/7.jpg'
                   },
                   {
                       path: '/images/gallery/turkey/8.jpg',
                       thumb: '/images/gallery/turkey/thumb/8.jpg'
                   },
                   {
                       path: '/images/gallery/turkey/9.jpg',
                       thumb: '/images/gallery/turkey/thumb/9.jpg'
                   },
                   {
                       path: '/images/gallery/turkey/10.jpg',
                       thumb: '/images/gallery/turkey/thumb/10.jpg'
                   },
                   {
                       path: '/images/gallery/turkey/11.jpg',
                       thumb: '/images/gallery/turkey/thumb/11.jpg'
                   },
                   {
                       path: '/images/gallery/turkey/12.jpg',
                       thumb: '/images/gallery/turkey/thumb/12.jpg'
                   },
                   {
                       path: '/images/gallery/turkey/13.jpg',
                       thumb: '/images/gallery/turkey/thumb/13.jpg'
                   },
                   {
                       path: '/images/gallery/turkey/14.jpg',
                       thumb: '/images/gallery/turkey/thumb/14.jpg'
                   },
                   {
                       path: '/images/gallery/turkey/15.jpg',
                       thumb: '/images/gallery/turkey/thumb/15.jpg'
                   },
                   {
                       path: '/images/gallery/turkey/16.jpg',
                       thumb: '/images/gallery/turkey/thumb/16.jpg'
                   }
               ]
           }
       ]
   };

    $scope.current = $scope.images.children;

    $scope.clickItem = function(index){
        if ($scope.isFolder($scope.current[index])){
            $scope.current = $scope.current[index].children;
        } else {
            $scope.isFullscreen = true;
            $scope.currentImage = $scope.current[index];
            $scope.currentImageIndex = index;
        }
    };

    $scope.isFullscreen = false;
    $scope.currentImage = null;
    $scope.currentImageIndex = null;

    $scope.closeFullscreen = function(){
        $scope.isFullscreen = false;
    };

    function SetImage(index){
        if (index < 0) return;
        if (index > $scope.current.length - 1) return;
        $scope.currentImage = $scope.current[index];
        $scope.currentImageIndex = index;
    }

    $scope.leftClick = function (){
          SetImage($scope.currentImageIndex - 1);
    };

    $scope.rightClick = function (){
        SetImage($scope.currentImageIndex + 1);
    };

    window.addEventListener("keydown", function (event) {
        if (event.keyCode == 27) {
            $scope.isFullscreen = false;
            $scope.$apply();
        }
    });

});

