var main = angular.module("main",[]);
main.controller('mainController', function ($scope) {
    function SetSize() {
        var kx = window.outerWidth / 1280;
        var ky = window.outerHeight / 720;
        var k = kx;
        if (ky < k) k = ky;
        $scope.fontSize = 14 * k;
    }

    $scope.url = '/pages/main.html';
    SetSize();

    window.addEventListener("resize", function () {
        SetSize();
        $scope.$apply();
    });
});

main.controller('menuController', function ($scope) {
    function SetPage(pageIndex) {
        var item = $scope.menu[pageIndex];
        $scope.$parent.url = item.url;
        $scope.active = pageIndex;
    }

    $scope.menu = [
        {   title: "Главная",
            url: '/pages/main.html'
        },
        {   title: "Новости",
            url: '/pages/news.html'
        },
        {   title: "Галерея",
            url: '/pages/gallery.html'
        },
        {   title: "Контакты",
            url: '/pages/info.html'
        }
    ];
    SetPage(0);
    $scope.menuClick = function(index) {
        SetPage(index);
    };
});
